from browser import document, html
from maison import *

import csv

with open('Questions.csv', mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    questions = [{key: value for key, value in element.items()} for
                         element in reader]

document <= html.IMG(src="choixpeau.png", id='choixpeau')
document <= html.H1('Trouvez votre maison à Poudlard en répondant à ce questionnaire !', id='titre')
document <= html.BUTTON('Commencer le questionnaire', id='depart')

x = 0
profil = []


def question_suivante(event):
    global profil, x     
    x += 1
    if x < len(questions):
        document['question'].textContent = questions[x]['Questions']
        if x == 3:
            document['choixpeau'].style.display = 'none'
            document <= html.IMG(src='Cloud.png', id='cloud')
        if x == 4:
            document['cloud'].style.display = 'none'
            document <= html.IMG(src="choixpeau.png", id='choixpeau')
        for i in range(1, 5):
            document['reponse'+str(i)].textContent = questions[x]['Réponse ' + str(i)]
        profil.append(eval(questions[x]["Tuple " + str(event.target.value)]))
    else:
            document['question'].style.display = 'none'
            for i in range(1, 5):
                document['reponse' + str(i)].style.display = 'none'
            courage = 0
            ambition = 0
            intelligence = 0
            good = 0
            for element in profil:
                for i in element:
                    i = int(i)
                courage += element[0]
                ambition += element[1]
                intelligence += element[2]
                good += element[3]
            courage = courage / 15
            ambition = ambition / 15
            intelligence = intelligence / 15
            good = good / 15
            moyenne_skills = {'Courage' : courage, 'Ambition' : ambition, 'Intelligence' : intelligence, 'Good' : good}
            house = counter(close_neighbours(distance(moyenne_skills, characters_infos), 5))[0]
            neighbours_dict = counter(close_neighbours(distance(moyenne_skills, characters_infos), 5))[1]
            neighbours = ''
            print(neighbours_dict)
            for key, value in neighbours_dict.items():
                neighbours += str(key) + ' (' + str(value) + '), \n'
            if house == 'Gryffindor':
                document <= html.IMG(src="Gryffindor.png", id='gryffondor')
            elif house == 'Slytherin':
                document <= html.IMG(src="Slytherin.png", id='serpentard')
            elif house == 'Ravenclaw':
                document <= html.IMG(src="Ravenclaw.png", id='serdaigle')
            else:
                document <= html.IMG(src="Hufflepuff.png", id='poufsouffle')
            document <= html.H2('Bravo ! Votre maison est ' + str(house))
            document <= html.H3('Vos plus proches voisins sont ' + neighbours)
        

def depart(event):
    global x
    document['titre'].style.display = 'none'
    document['depart'].style.display = 'none'
    document <= html.P(questions[x]['Questions'], id='question')
    for i in range(1, 5):
        document <= html.P(html.BUTTON(questions[x]['Réponse ' + str(i)], id='reponse'+str(i), value=i))
        document['reponse'+str(i)].bind("click", question_suivante)
                  

document['depart'].bind("click", depart)
